#!/bin/bash

# 1. compile

  gfortran -O3                      \
           -Wall                    \
           -std=f2008               \
           -static                  \
           m_1_type_definitions.f90 \
           m_2_mean_pack.f90        \
           m_3_mean_do_loop.f90     \
           driver_program.f90       \
           -o x_gnu

# 2. clean

  rm *.mod
