!===============================!
! Author: Pavlos G. Galiatsatos !
! Date: 2013/08/05              !
!===============================!

  module m_2_mean_pack

  contains

!================================================!
! 1. subroutine: mean_pack(data_array, mean_val) !
!================================================!

  subroutine mean_pack(data_array, mean_val)

  use m_1_type_definitions
  
  implicit none

  real(kind=dr), dimension(:) :: data_array
  real(kind=dr)               :: mean_val

  mean_val = sum(data_array)/max(1_si, size(data_array))

  end subroutine mean_pack

  end module m_2_mean_pack

!======!
! FINI !
!======!
