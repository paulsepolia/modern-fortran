
  program driver_program

  use m_1_type_definitions
  use m_2_mean_pack
  use m_3_mean_do_loop
 
  implicit none

  !  1. local parameters

  integer(kind=si), parameter :: MAX_DO = 7_si

  ! 2. local variables

  real(kind=dr), dimension(:), allocatable :: data_array
  real(kind=dr)                            :: threshold
  real(kind=dr)                            :: mean1
  real(kind=dr)                            :: mean2
  real(kind=dr)                            :: difference
  real(kind=dr)                            :: mean_sum
  integer(kind=si)                         :: i
  integer(kind=si)                         :: j
  integer(kind=si)                         :: no_data
  integer(kind=si)                         :: no_repeat
  integer(kind=si)                         :: time1, time2, time3
  integer(kind=si), dimension(1:MAX_DO)    :: time_pack
  integer(kind=si), dimension(1:MAX_DO)    :: time_do

  ! 3. initialization 

  threshold = 0.1_dr
  no_data   = 1_si
  no_repeat = 1000_si

  ! 4. main benchmark 

  do i = 1, MAX_DO

    no_data = no_data * 10_si
        
    allocate(data_array(1:no_data))

    call random_number(data_array)
        
    difference = 0.0_dr
    mean_sum   = 0.0_dr
       
    call system_clock(time1)
 
    do j = 1, no_repeat
            
      call mean_pack(pack(data_array, data_array > threshold), mean1)
        
      mean_sum = mean_sum + mean1
        
    end do
    
    call system_clock(time2)
 
    do j = 1, no_repeat
            
      call mean_do_loop(data_array, threshold, mean2)
      
      mean_sum = mean_sum + mean2

    enddo
 
    call system_clock(time3)

    time_pack(i) = time_pack(i) + (time2-time1)
    time_do(i)   = time_do(i) + (time3-time2)
    difference   = difference + abs(mean1-mean2)

    deallocate(data_array)
 
  end do

  ! 5. output the results

  write(*, '(i10, 2i10)') (10**i, time_do(i), time_pack(i), i = 1,MAX_DO)
  write(*, '(a, e12.4)')  'Mean difference: ', difference / 600.0_dr

  ! 6. end of code

  end program driver_program

!======!
! FINI !
!======!
