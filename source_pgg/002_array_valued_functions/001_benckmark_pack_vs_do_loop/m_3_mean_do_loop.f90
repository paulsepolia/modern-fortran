!===============================!
! Author: Pavlos G. Galiatsatos !
! Date: 2013/08/05              !
!===============================!

  module m_3_mean_do_loop

  contains

!==============================================================!
! 1. subroutine: mean_do_loop(data_array, threshold, mean_val) !
!==============================================================!

  subroutine mean_do_loop(data_array, threshold, mean_val)

  use m_1_type_definitions

  implicit none

  real(kind=dr), dimension(:), allocatable :: data_array
  real(kind=dr)                            :: threshold
  real(kind=dr)                            :: mean_val
  integer(kind=si)                         :: i
  integer(kind=si)                         :: count_int

  mean_val = 0.0_dr
  count_int = 0_si

  do i = 1, size(data_array)
    if (data_array(i) > threshold) then
      mean_val  = mean_val + data_array(i)
      count_int = count_int + 1_si
    endif
  enddo

  mean_val = mean_val/max(1_si, count_int)

  end subroutine mean_do_loop

  end module m_3_mean_do_loop

!======!
! FINI !
!======!
