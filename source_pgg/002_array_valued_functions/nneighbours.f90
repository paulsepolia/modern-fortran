! nneighbours.f90 --
!
!     Example belonging to "Modern Fortran in Practice" by Arjen Markus
!
!     This work is licensed under the Creative Commons Attribution 3.0 Unported License.
!     To view a copy of this license, visit http://creativecommons.org/licenses/by/3.0/
!     or send a letter to:
!     Creative Commons, 444 Castro Street, Suite 900, Mountain View, California, 94041, USA.
!
!     Illustrate a number-theoretic problem concerning multiples
!     of irrational number (using array-valued functions)
!
program nneighbours
    implicit none
    integer :: i

    write(*,'(f10.4)') cdiff( sort( (/ (mod(i*sqrt(2.0),1.0) ,i=1,20) /) ) )

contains

function cdiff( array )
    real, dimension(:) :: array
    real, dimension(size(array)) :: cdiff

    cdiff = abs( array - cshift(array,1) )
    cdiff = min( cdiff, 1.0 - cdiff )
end function

function sort( array )
    real, dimension(:)           :: array
    real, dimension(size(array)) :: sort

    real                         :: temp
    integer                      :: i, j
    integer, dimension(1)        :: pos

    !
    ! Retrieve the lowest elements one by one
    !
    sort = array
    do i = 1,size(sort)
        pos     = minloc( sort(i:) )
        j       = i + pos(1) - 1
        temp    = sort(j)
        sort(j) = sort(i)
        sort(i) = temp
    enddo
end function
end program
