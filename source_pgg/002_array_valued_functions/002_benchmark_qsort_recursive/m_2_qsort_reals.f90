!===============================!
! Author: Pavlos G. Galiatsatos !
! Date: 2013/08/06              !
!===============================!

  module m_2_qsort_reals
  
  implicit none

  contains

  ! 1. function: qsort_reals

  recursive function qsort_reals(data_array) result(sorted_array)

  use m_1_type_definitions
   
  implicit none
 
  real(kind=dr), dimension(:), intent(in)      :: data_array
  real(kind=dr), dimension(1:size(data_array)) :: sorted_array

  if (size(data_array) > 1_si) then
    
    sorted_array = &
                 (/ qsort_reals(pack(data_array(2:), data_array(2:) > data_array(1))),   &
                    data_array(1),                                           &
                    qsort_reals(pack(data_array(2:), data_array(2:) <= data_array(1))) /)
  else

    sorted_array = data_array

  endif

  end function qsort_reals

  end module m_2_qsort_reals

!======!
! FINI !
!======!
