#!/bin/bash

# 1. compile

  gfortran -O3                      \
           -Wall                    \
           -std=f2008               \
           -static                  \
           m_1_type_definitions.f90 \
           m_2_qsort_reals.f90      \
           driver_program.f90       \
           -o x_gnu

# 2. clean

  rm *.mod
