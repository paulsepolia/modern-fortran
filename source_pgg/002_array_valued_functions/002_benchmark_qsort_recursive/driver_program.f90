!===============================!
! Author: Pavlos G. Galiatsatos !
! Date: 2013/08/06              !
!===============================!

  program driver_program

  use m_1_type_definitions
  use m_2_qsort_reals
 
  implicit none

  !  1. local parameteres

  integer(kind=si), parameter :: ARRAY_DIM = 40000000_si

  !  2. local variables

  integer(kind=si)                         :: i
  real(kind=dr), dimension(:), allocatable :: data_array
  real(kind=dr), dimension(:), allocatable :: sorted_array

  !  3. allocation of RAM

  allocate(data_array(1:ARRAY_DIM))
  allocate(sorted_array(1:ARRAY_DIM))

  !  4. initialization

  call random_number(data_array)

  sorted_array = 0.0_dr

  !  5. function call qsort_reals

  sorted_array = qsort_reals(data_array)

  !  6. some output

  do i = 1, 10
    write(*,*) i, " --> ",  sorted_array(i)
  end do

  do i = ARRAY_DIM-10, ARRAY_DIM
    write(*,*) i, " --> ",  sorted_array(i)
  end do

  end program driver_program

!======!
! FINI !
!======!
