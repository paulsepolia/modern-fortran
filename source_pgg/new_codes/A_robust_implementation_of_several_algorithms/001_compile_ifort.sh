#!/bin/bash

# 1. compile

  ifort -O3                            \
        -w                             \
        -e08                           \
        -static                        \
        -static-intel                  \
        -assume buffered_io            \
        m_1_type_definitions.f90       \
        driver_program_robust_stat.f90 \
        -o x_intel

# 2. clean

  rm *.mod
