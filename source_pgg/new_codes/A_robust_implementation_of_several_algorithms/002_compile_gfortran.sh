#!/bin/bash

# 1. compile

  gfortran-4.8.0 -O3                            \
                 -Wall                          \
                 -std=f2008                     \
                 -static                        \
                 m_1_type_definitions.f90       \
                 driver_program_robust_stat.f90 \
                 -o x_gnu

# 2. clean

  rm *.mod
