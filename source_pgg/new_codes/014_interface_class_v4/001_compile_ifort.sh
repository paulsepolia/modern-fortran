#!/bin/bash

# 1. compile

  ifort -O3                    \
        -w                     \
        -e08                   \
        -static                \
        -static-intel          \
        integrate_class_v4.f90 \
        -o x_intel

# 2. clean

  rm *.mod
