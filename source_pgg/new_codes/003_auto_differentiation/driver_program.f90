!==========================!
! derivative_near_singular !
!==========================!

  program driver_program

  use m_1_type_definitions
  use m_2_automatic_differentiation_type
  use m_3_automatic_differentiation

  implicit none

  type(autoderiv)  :: x
  type(autoderiv)  :: result_fun
  real(kind=dr)    :: approx
  integer(kind=si) :: i

  real(kind=dr), dimension(10) :: value_mat = &
  (/ 0.00100_dr, 0.00050_dr, 0.000200_dr, 0.000100_dr, 0.000050_dr, &
     0.00002_dr, 0.00001_dr, 0.000005_dr, 0.000002_dr, 0.000001_dr /)

  do i = 1,size(value_mat)
    x%v  = 1.0_dr + value_mat(i)
    x%dv = 1.0_dr

    result_fun = f(x)
    approx = fprime(x%v)

    write(*,'(10f12.8)') x%v, result_fun%v, result_fun%dv, approx, &
                         result_fun%dv+0.5_dr+2.0_dr*(x%v-1.0_dr)/3.0_dr, &
                         approx+0.5_dr+2.0_dr*(x%v-1.0_dr)/3.0_dr
  end do

!==========!
! contains !
!==========!

  contains

  ! 1.

  function f(x)
    type(autoderiv), intent(in) :: x
    type(autoderiv)             :: f

    f = log(x) / (x-1.0_dr)
  end function f

  ! 2.

  real(kind=dr) function fprime(x)
    real(kind=dr) :: x
    real(kind=dr) :: eps

    eps    = x - 1.0_dr
    fprime = (-0.5_dr + eps / 6.0_dr - eps**2 / 12.0_dr + eps**3 / 20.0_dr) / x
  end function fprime

  end program driver_program
