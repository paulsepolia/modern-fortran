!===============================!
! Author: Pavlos G. Galiatsatos !
! Date: 2013/08/05              !
!===============================!

  module m_3_automatic_differentiation
 
  use m_1_type_definitions
  use m_2_automatic_differentiation_type
  
  implicit none

  !  1.

  interface sin
    module procedure sin_v
  end interface

  !  2.

  interface cos
    module procedure cos_v
  end interface

  !  3.

  interface tan
    module procedure tan_v
  end interface

  !  4.

  interface exp
    module procedure exp_v
  end interface

  !  5.

  interface log
    module procedure log_v
  end interface

  !  6.

  interface log10
    module procedure log10_v
  end interface

  !  7.

  interface sqrt
    module procedure sqrt_v
  end interface

  !  8.

  interface asin
    module procedure asin_v
  end interface

  !  9.

  interface acos
    module procedure acos_v
  end interface

  ! 10.

  interface atan
    module procedure atan_v
  end interface

  ! 11.

  interface sinh
    module procedure sinh_v
  end interface

  ! 12.

  interface tanh
    module procedure tanh_v
  end interface

  ! 13.

  interface cosh
    module procedure cosh_v
  end interface

  ! 14.

  interface operator(+)
    module procedure add_cv
    module procedure add_vc
    module procedure add_vv
  end interface

  ! 15.

  interface operator(-)
    module procedure sub_cv
    module procedure sub_vc
    module procedure sub_vv
    module procedure usub_v  ! Unary minus!
    end interface

  ! 16.

  interface operator(*)
    module procedure mult_cv
    module procedure mult_vc
    module procedure mult_vv
  end interface

  ! 17.

  interface operator(/)
    module procedure div_cv
    module procedure div_vc
    module procedure div_vv
  end interface

  ! 18.

  interface operator(**)
    module procedure expon_cv
    module procedure expon_vc
    module procedure expon_vv
    module procedure expon_vn ! x ** n
  end interface

!==========!
! contains !
!==========!

  contains

  ! 19.

  function derivvar(val) result(x)
    real(kind=dr)   :: val
    type(AUTODERIV) :: x

    x%v  = val
    x%dv = 1.0_dr
  end function derivvar

  ! 20.

  elemental function sin_v(x) result(y)
    type(AUTODERIV), intent(in) :: x
    type(AUTODERIV)             :: y

    y%v  = sin(x%v)
    y%dv = x%dv * cos(x%v)
  end function sin_v

  ! 21.

  elemental function cos_v(x) result(y)
    type(AUTODERIV), intent(in) :: x
    type(AUTODERIV)             :: y

    y%v  = cos(x%v)
    y%dv = -x%dv * sin(x%v)
  end function cos_v

  ! 22.

  elemental function tan_v(x) result(y)
    type(AUTODERIV), intent(in) :: x
    type(AUTODERIV)             :: y

    y%v  = tan(x%v)
    y%dv = x%dv / (1.0_dr + y%v**2_si)
  end function tan_v

  ! 23.

  elemental function exp_v(x) result(y)
    type(AUTODERIV), intent(in) :: x
    type(AUTODERIV)             :: y

    y%v  = exp(x%v)
    y%dv = x%dv * y%v
  end function exp_v

  ! 24.

  elemental function log_v(x) result(y)
    type(AUTODERIV), intent(in) :: x
    type(AUTODERIV)             :: y

    y%v  = log(x%v)
    y%dv = x%dv / x%v
  end function log_v

  ! 25.

  elemental function log10_v(x) result(y)
    type(AUTODERIV), intent(in) :: x
    type(AUTODERIV)             :: y

    y%v  = log10(x%v)
    y%dv = x%dv / (log(10.0_dr) * x%v)
  end function log10_v

  ! 26.

  elemental function sqrt_v(x) result(y)
    type(AUTODERIV), intent(in) :: x
    type(AUTODERIV)             :: y

    y%v  = sqrt(x%v)
    y%dv = 0.5_dr *x%dv / y%v
  end function sqrt_v

  ! 27.

  elemental function sinh_v(x) result(y)
    type(AUTODERIV), intent(in) :: x
    type(AUTODERIV)             :: y

    y%v  = sinh(x%v)
    y%dv = x%dv * cosh(x%v)
  end function sinh_v

  ! 28.

  elemental function cosh_v(x) result(y)
    type(AUTODERIV), intent(in) :: x
    type(AUTODERIV)             :: y

    y%v  = cosh(x%v)
    y%dv = x%dv * sinh(x%v)
  end function cosh_v

  ! 29.

  elemental function tanh_v(x) result(y)
    type(AUTODERIV), intent(in) :: x
    type(AUTODERIV)             :: y

    y%v  = tanh(x%v)
    y%dv = x%dv * (1.0_dr - y%v**2_si)
  end function tanh_v

  ! 30.

  elemental function asin_v(x) result(y)
    type(AUTODERIV), intent(in) :: x
    type(AUTODERIV)             :: y

    y%v  = asin(x%v)
    y%dv = x%dv * sqrt(1.0_dr - x%v ** 2_si)
  end function asin_v

  ! 31.

  elemental function acos_v(x) result(y)
    type(AUTODERIV), intent(in) :: x
    type(AUTODERIV)             :: y

    y%v  = acos(x%v)
    y%dv = -x%dv * sqrt(1.0_dr - x%v ** 2_si)
  end function acos_v

  ! 32.

  elemental function atan_v(x) result(y)
    type(AUTODERIV), intent(in) :: x
    type(AUTODERIV)             :: y

    y%v  = atan(x%v)
    y%dv = x%dv / (1.0_dr + x%v ** 2_si)
  end function atan_v

  ! 33.

  elemental function add_vv(x, y) result(z)
    type(AUTODERIV), intent(in) :: x
    type(AUTODERIV), intent(in) :: y
    type(AUTODERIV)             :: z

    z%v  = x%v  + y%v
    z%dv = x%dv + y%dv
  end function add_vv

  ! 34.

  elemental function add_cv(x, y) result(z)
    real(kind=dr), intent(in)   :: x
    type(AUTODERIV), intent(in) :: y
    type(AUTODERIV)             :: z

    z%v  = x    + y%v
    z%dv = y%dv
  end function add_cv

  ! 35.

  elemental function add_vc(x, y) result(z)
    type(AUTODERIV), intent(in) :: x
    real(kind=dr), intent(in)   :: y
    type(AUTODERIV)             :: z

    z%v  = x%v  + y
    z%dv = x%dv
  end function add_vc

  ! 36.
  
  elemental function usub_v(x) result(z)
    type(AUTODERIV), intent(in) :: x
    type(AUTODERIV)             :: z

    z%v  = -x%v
    z%dv = -x%dv
  end function usub_v

  ! 37.

  elemental function sub_vv(x,y) result(z)
    type(AUTODERIV), intent(in) :: x
    type(AUTODERIV), intent(in) :: y
    type(AUTODERIV)             :: z

    z%v  = x%v  - y%v
    z%dv = x%dv - y%dv
  end function sub_vv

  ! 38.

  elemental function sub_cv(x, y) result(z)
    real(kind=dr), intent(in)   :: x
    type(AUTODERIV), intent(in) :: y
    type(AUTODERIV)             :: z

    z%v  = x  - y%v
    z%dv =    - y%dv
  end function sub_cv

  ! 39.

  elemental function sub_vc(x, y) result(z)
    type(AUTODERIV), intent(in) :: x
    real(kind=dr), intent(in)   :: y
    type(AUTODERIV)             :: z

    z%v  = x%v  - y
    z%dv = x%dv
  end function sub_vc

  ! 40.

  elemental function mult_vv(x, y) result(z)
    type(AUTODERIV), intent(in) :: x
    type(AUTODERIV), intent(in) :: y
    type(AUTODERIV)             :: z

    z%v  = x%v  * y%v
    z%dv = x%dv * y%v + x%v * y%dv
  end function mult_vv

  ! 41.

  elemental function mult_cv(x, y) result(z)
    real(kind=dr), intent(in)   :: x
    type(AUTODERIV), intent(in) :: y
    type(AUTODERIV)             :: z

    z%v  = x * y%v
    z%dv = x * y%dv
  end function mult_cv

  ! 42.

  elemental function mult_vc(x, y) result(z)
    type(AUTODERIV), intent(in) :: x
    real(kind=dr), intent(in)   :: y
    type(AUTODERIV)             :: z

    z%v  = x%v  * y
    z%dv = x%dv * y
  end function mult_vc

  ! 43.

  elemental function div_vv(x, y) result(z)
    type(AUTODERIV), intent(in) :: x
    type(AUTODERIV), intent(in) :: y
    type(AUTODERIV)             :: z

    z%v  = x%v  / y%v
    z%dv = (x%dv * y%v - x%v * y%dv) / y%v ** 2_si
  end function div_vv

  ! 44.

  elemental function div_cv(x, y) result(z)
    real(kind=dr), intent(in)   :: x
    type(AUTODERIV), intent(in) :: y
    type(AUTODERIV)             :: z

    z%v  = x / y%v
    z%dv = -x * y%dv / y%v ** 2_si
  end function div_cv

  ! 45.

  elemental function div_vc(x, y) result(z)
    type(AUTODERIV), intent(in) :: x
    real(kind=dr), intent(in)   :: y
    type(AUTODERIV)             :: z

    z%v  = x%v  / y
    z%dv = x%dv / y
  end function div_vc

  ! 46.

  elemental function expon_vv(x, y) result(z)
    type(AUTODERIV), intent(in) :: x
    type(AUTODERIV), intent(in) :: y
    type(AUTODERIV)             :: z

    z%v  = x%v  ** y%v
    z%dv = (y%dv * log(x%v) + y%v/x%v * x%dv ) * z%v
  end function expon_vv

  ! 47.

  elemental function expon_cv(x, y) result(z)
    real(kind=dr), intent(in)   :: x
    type(AUTODERIV), intent(in) :: y
    type(AUTODERIV)             :: z

    z%v  = x ** y%v
    z%dv = y%dv * log(x) * z%v
  end function expon_cv

  ! 48.

  elemental function expon_vc(x, y) result(z)
    type(AUTODERIV), intent(in) :: x
    real(kind=dr), intent(in)   :: y
    type(AUTODERIV)             :: z

    z%v  = x%v  ** y
    z%dv = y/x%v * x%dv * z%v
  end function expon_vc

  ! 49.

  elemental function expon_vn(x, n) result(z)
    type(AUTODERIV), intent(in)  :: x
    integer(kind=si), intent(in) :: n
    type(AUTODERIV)              :: z

    z%v  =            x%v ** n
    z%dv = n * x%dv * x%v ** (n-1)
  end function expon_vn

  ! 50.

!===========================================================!
! find_root                                                 !
!     Find a root using Newton-Raphson                      !
! Arguments:                                                !
!     f          Function whose root we want to find        !
!     xinit      Initial estimate                           !
!     tolerance  Tolerance in finding root                  !
!     root       Final estimate                             !
!     found      Whether it was found or not                !
! Note:                                                     !
!     If the iteration does not converge, we assume         !
!     that the iterate will grow indefinitely.              !
!                                                           !
!     If you need a more general interface to the function, !
!     consider using the implementation as a template       !
!===========================================================!
  
  subroutine find_root(f, xinit, tolerance, root, found)
    interface
      function f(x)
        use m_2_automatic_differentiation_type
        type(AUTODERIV), intent(in) :: x
        type(AUTODERIV)             :: f
      end function
    end interface

    type(AUTODERIV), intent(in)  :: xinit
    type(AUTODERIV), intent(out) :: root
    real(kind=dr)                :: tolerance
    logical                      :: found

    integer(kind=si)             :: iter
    integer(kind=si), parameter  :: maxiter = 1000_si
    type(AUTODERIV)              :: fvalue

    found = .false.
    root  = xinit

    do iter = 1, maxiter

      fvalue = f(root)
      call find_root_iter(fvalue, root, tolerance, found)
      if (found .or. abs(root%v) > huge(1.0_dr)/10.0_dr) exit
    end do

  end subroutine find_root

  ! 51.
 
!===================================================!
! find_root_iter                                    !
!     Do one iteration in the Newton-Raphson method !
! Arguments:                                        !
!     fvalue     Function value (plus derivative)   !
!     root       Current iterate                    !
!     tolerance  Tolerance in finding root          !
!     found      Whether it was found or not        !
!===================================================!

  subroutine find_root_iter( fvalue, root, tolerance, found )
    type(AUTODERIV), intent(in)    :: fvalue
    type(AUTODERIV), intent(inout) :: root
    real(kind=dr), intent(in)      :: tolerance
    logical, intent(out)           :: found
    type(AUTODERIV)                :: newroot

    newroot = root - fvalue%v / fvalue%dv
    found   = abs(newroot%v - root%v) < tolerance
    root    = newroot

  end subroutine find_root_iter

  end module m_3_automatic_differentiation

!======!
! FINI !    
!======!
