#!/bin/bash

# 1. compile

  gfortran-4.8.0 -O3                                    \
                 -Wall                                  \
                 -std=f2008                             \
                 -static                                \
                 m_1_type_definitions.f90               \
                 m_2_automatic_differentiation_type.f90 \
                 m_3_automatic_differentiation.f90      \
                 driver_program.f90                     \
                 -o x_gnu

# 2. clean

  rm *.mod
