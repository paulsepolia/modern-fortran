#!/bin/bash

# 1. compile

  ifort -O3                                    \
        -w                                     \
        -e08                                   \
        -static                                \
        -static-intel                          \
        m_1_type_definitions.f90               \
        m_2_automatic_differentiation_type.f90 \
        m_3_automatic_differentiation.f90      \
        driver_program.f90                     \
        -o x_intel

# 2. clean

  rm *.mod
