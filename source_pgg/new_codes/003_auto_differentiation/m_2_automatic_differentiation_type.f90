!===============================!
! Author: Pavlos G. Galiatsatos !
! Date: 2013/08/06              !
!===============================!

  module m_2_automatic_differentiation_type
 
  use m_1_type_definitions
 
  implicit none

  type AUTODERIV
    real(kind=dr) :: v
    real(kind=dr) :: dv
  end type AUTODERIV

  end module m_2_automatic_differentiation_type

!======!
! FINI !
!======!
