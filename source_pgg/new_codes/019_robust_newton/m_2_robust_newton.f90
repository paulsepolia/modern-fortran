!=============================================!
! robust_newton.f90                           !
! Robust version of the Newton-Raphson method !
!=============================================!

  module m_2_robust_newton

  use m_1_type_definitions

  implicit none

  integer(kind=si), parameter :: bracketed_root       =  1_si
  integer(kind=si), parameter :: small_value_solution =  2_si
  integer(kind=si), parameter :: convergence_reached  =  3_si
  integer(kind=si), parameter :: invalid_start_value  = -1_si
  integer(kind=si), parameter :: no_convergence       = -2_si
  integer(kind=si), parameter :: invalid_arguments    = -3_si

  contains

  subroutine find_root(f, xinit, scalex, tolx, smallf, maxevals, result_var, success)

  use m_1_type_definitions

  implicit none

  !  1. interface

  interface

    subroutine f(x, indomain, value_var)

    use m_1_type_definitions

    implicit none

    real(kind=dr), intent(in)  :: x
    logical, intent(out)       :: indomain
    real(kind=dr), intent(out) :: value_var

    end subroutine f

  end interface

  !  2. local and interface variables

  real(kind=dr), intent(in)     :: xinit
  real(kind=dr), intent(in)     :: scalex
  real(kind=dr), intent(in)     :: tolx
  real(kind=dr), intent(in)     :: smallf
  integer(kind=si), intent(in)  :: maxevals
  real(kind=dr), intent(out)    :: result_var
  integer(kind=si), intent(out) :: success
  real(kind=dr)                 :: eps
  real(kind=dr)                 :: epsinit
  real(kind=dr)                 :: epsf
  real(kind=dr)                 :: fx1
  real(kind=dr)                 :: fx2
  real(kind=dr)                 :: fxnew
  real(kind=dr)                 :: fprime
  real(kind=dr)                 :: x
  real(kind=dr)                 :: xnew
  integer(kind=si)              :: evals
  logical                       :: indomain

  result_var  = 0.0_dr
  success = no_convergence

  !
  ! Sanity check
  !

  if (scalex <= 0.0_dr .or. tolx <= 0.0_dr .or. smallf <= 0.0_dr) then
    success = invalid_arguments
    return
  end if

  !
  ! Starting value for the stepsize
  !

  epsinit = scalex * sqrt(epsilon(xinit))
  epsf    =  100.0_dr * epsilon(fx1)

  !
  ! Check the initial value
  !

  x = xinit

  call f(x, indomain, fx1)
  evals = 1_si

  if (.not. indomain) then
    success = invalid_start_value
    return
  end if

  outerloop: &
  do
    !
    ! Is the function value small enough?
    ! Then stop
    !
    if (abs(fx1) <= smallf) then
      success = small_value_solution
      result_var  = x
      exit
    end if

    !
    ! Determine the derivative - be careful about the domain
    !
    eps = epsinit

    epsloop: &
    do while (evals < maxevals)
      call f(x+eps, indomain, fx2)
      write(*,*) 'Eps: ', eps

      evals = evals + 1_si
      if (evals >= maxevals) exit

      if (.not. indomain) then
        eps = -eps
        call f(x+eps, indomain, fx2)

        evals = evals + 1_si
        if (evals >= maxevals) exit outerloop

        if (.not. indomain) exit outerloop
      endif

      if (abs(fx2-fx1) < epsf * (abs(fx2)+abs(fx1))/2.0_dr + smallf) then
      eps = 2.0_dr * eps
      else
        exit epsloop
      end if
    end do epsloop

    write(*,*) evals, fx1, fx2, eps

    fprime = (fx2 - fx1) / eps

    !
    ! Determine the next estimate
    !

    newxloop: &
    do while (evals < maxevals)
      xnew  = x - fx1 / fprime

      call f( xnew, indomain, fxnew )
      evals = evals + 1_si

      if (.not. indomain) then
        fx1  = fx1 / 2.0_dr
      else
        exit newxloop
      end if
    enddo newxloop

    fx1 = fxnew

    !
    ! Have we reached convergence?
    !
    write(*,*) 'Difference: ', abs(xnew-x), ' -- ', scalex * tolx
    if (evals < maxevals) then
      if (abs(xnew-x) <= scalex * tolx) then
        success = convergence_reached
        if (abs(fx1) < smallf) then
          success = small_value_solution
        end if
        result_var = xnew
        write(*,*) 'Result: ',result_var
        exit outerloop
      end if
    else
      exit outerloop
    end if

    x = xnew
    write(*,*) evals, x
  end do outerloop

  !
  ! Simply a small value or a bracketed root?
  !
  call f(x - scalex*tolx, indomain, fx1)
  evals = evals + 1_si

  if (indomain) then
    call f(x + scalex*tolx, indomain, fx2)
    evals = evals + 1_si
    if (indomain) then
      if (fx1 * fx2 <= 0.0_dr) then
        success = bracketed_root
      end if
    end if
  end if
  
  write(*,*) 'Number of evaluations: ', evals

  end subroutine find_root

  end module m_2_robust_newton

!======!
! FINI !
!======!
