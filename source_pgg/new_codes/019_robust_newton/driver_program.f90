
  program test_newton
    
  use m_1_type_definitions
  use m_2_robust_newton
  use m_3_functions

  implicit none

  real(kind=dr)    :: x
  real(kind=dr)    :: root
  real(kind=dr)    :: froot
  integer(kind=si) :: maxiter = 20_si
  integer(kind=si) :: conclusion
  logical          :: indomain

  write(*,*) 'fln: 0.1'
  x = 0.1_dr
  call find_root(fln, x, 1.0_dr, 1.0e-5_dr, 1.0e-5_dr, maxiter, root, conclusion)
  call fln(root, indomain, froot)
  write(*,*) root, froot, conclusion

  write(*,*) 'fln: 10.0'
  x = 10.0_dr
  call find_root(fln, x, 1.0_dr, 1.0e-5_dr, 1.0e-5_dr, maxiter, root, conclusion)
  call fln(root, indomain, froot)
  write(*,*) root, froot, conclusion

  write(*,*) 'fparabola: 10.0'
  x = 10.0_dr
  call find_root(fparabola, x, 1.0_dr, 1.0e-5_dr, 1.0e-5_dr, 2_si*maxiter, root, conclusion)
  call fparabola(root, indomain, froot )
  write(*,*) root, froot, conclusion

  write(*,*) 'fparabola2: 10.0'
  x = 10.0_dr
  call find_root(fparabola2, x, 1.0_dr, 1.0e-5_dr, 1.0e-5_dr, maxiter, root, conclusion)
  call fparabola2(root, indomain, froot)
  write(*,*) root, froot, conclusion

  write(*,*) 'fsqrt: 1.0'
  x = 1.0_dr
  call find_root(fsqrt, x, 1.0_dr, 1.0e-5_dr, 1.0e-5_dr, maxiter, root, conclusion)
  call fsqrt(root, indomain, froot )
  write(*,*) root, froot, conclusion

  write(*,*) 'fcos: 0.0'
  x = 0.0_dr
  call find_root(fcos, x, 1.0e-4_dr, 1.0e-5_dr, 1.0e-5_dr, 100_si*maxiter, root, conclusion)
  call fsqrt(root, indomain, froot)
  write(*,*) root, froot, conclusion

  write(*,*) 'fcos: 0.75e-4*pi'
  x = 0.75e-4_dr*3.14_dr
  call find_root( fcos, x, 1.0e-4_dr, 1.0e-5_dr, 1.0e-5_dr, 100_si*maxiter, root, conclusion)
  call fsqrt(root, indomain, froot)
  write(*,*) root, froot, conclusion

  end program test_newton

!======!
! FINI !
!======!
