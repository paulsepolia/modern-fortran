#!/bin/bash

# 1. compile

  ifort -O3                      \
        -w                       \
        -e08                     \
        -static                  \
        -static-intel            \
        -assume buffered_io      \
        m_1_type_definitions.f90 \
        m_2_robust_newton.f90    \
        m_3_functions.f90        \
        driver_program.f90       \
        -o x_intel

# 2. clean

  rm *.mod
