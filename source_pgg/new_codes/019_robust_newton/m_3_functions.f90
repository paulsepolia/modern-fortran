!==================!
! module functions !
!==================!

  module m_3_functions

  implicit none

  contains
 
  ! sub: f1
  
  subroutine f1(x, indomain, result_var )

  use m_1_type_definitions

  implicit none

  real(kind=dr), intent(in)  :: x
  logical, intent(out)       :: indomain
  real(kind=dr), intent(out) :: result_var

  indomain = .true.
  result_var = exp(x) + x  ! exp(x)-x is also interesting

  end subroutine

  ! sub: f2

  subroutine f2(x, indomain, result_var)

  use m_1_type_definitions

  implicit none

  real(kind=dr), intent(in)  :: x
  logical, intent(out)       :: indomain
  real(kind=dr), intent(out) :: result_var

  indomain = .true.
  result_var = x**2_si + 1.0_dr

  end subroutine f2

  ! sub: f3

  subroutine f3(x, indomain, result_var)
  
  use m_1_type_definitions

  implicit none

  real(kind=dr), intent(in)  :: x
  logical, intent(out)       :: indomain
  real(kind=dr), intent(out) :: result_var

  indomain = .true.

  ! f3 = sqrt(abs(x)) + 1.0    - cycle: 12.4 -- -130.08
  ! f3 = sqrt(abs(x)) + 0.1    - slow divergence
  ! f3 = sqrt(abs(x)) + 0.001  - cycle: 1.003 -- 1.004 -- 1.003 -- -1.004

  result_var = sqrt(abs(x)) + 0.001_dr

  end subroutine f3

  ! sub: f4

  subroutine f4(x, indomain, result_var)
 
  use m_1_type_definitions

  implicit none  

  real(kind=dr), intent(in)  :: x
  logical, intent(out)       :: indomain
  real(kind=dr), intent(out) :: result_var

  indomain = .true.
  result_var = abs(x) ** 0.3333_dr

  if ( x > 0.0_dr ) then
    result_var = result_var - 1.0_dr
  else
    result_var = -result_var - 1.0_dr
  end if

  end subroutine f4

  ! sub: fln

  subroutine fln(x, indomain, result_var)
 
  use m_1_type_definitions

  implicit none  

  real(kind=dr), intent(in)  :: x
  logical, intent(out)       :: indomain
  real(kind=dr), intent(out) :: result_var

  indomain = .true.
 
  if (x <= 0.0_dr) then
    indomain = .false.
    result_var = 0.0_dr
  else
    result_var = log(x) - 1.0_dr
  end if

  end subroutine fln

  ! sub: fparabola

  subroutine fparabola(x, indomain, result_var)

  use m_1_type_definitions

  implicit none

  real(kind=dr), intent(in)  :: x
  logical, intent(out)       :: indomain
  real(kind=dr), intent(out) :: result_var

  indomain = .true.
  result_var = x ** 2_si

  end subroutine fparabola

  ! sub: fparabola2

  subroutine fparabola2(x, indomain, result_var)
    
  use m_1_type_definitions

  implicit none

  real(kind=dr), intent(in)  :: x
  logical, intent(out) :: indomain
  real(kind=dr), intent(out) :: result_var

  indomain = .true.
  result_var = x ** 2_si + 1.0_dr

  end subroutine fparabola2

  ! sub: fsqrt

  subroutine fsqrt(x, indomain, result_var)
 
  use m_1_type_definitions

  implicit none

  real(kind=dr), intent(in)  :: x
  logical, intent(out)       :: indomain
  real(kind=dr), intent(out) :: result_var

  indomain = .true.
  result_var = sqrt(abs(x))

  end subroutine fsqrt

  ! sub: fcos

  subroutine fcos(x, indomain, result_var)

  use m_1_type_definitions

  implicit none

  real(kind=dr), intent(in)  :: x
  logical, intent(out)       :: indomain
  real(kind=dr), intent(out) :: result_var

  indomain = .true.
  result_var = cos(1.0e4*x)

  end subroutine fcos

  end module m_3_functions

!======!
! FINI !
!======!
