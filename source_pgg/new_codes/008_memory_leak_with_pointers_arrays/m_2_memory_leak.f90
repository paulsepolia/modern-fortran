!===============================!
! Author: Pavlos G. Galiatsatos !
! Date: 2013/08/07              !
!===============================!

  module m_2_memory_leak

  use m_1_type_definitions

  implicit none

  contains

  !=========================================!
  ! 1. subroutine: memory_leak_with_pointer !
  !=========================================!

  subroutine memory_leak_with_pointer(data_size)

  use m_1_type_definitions

  implicit none

  integer(kind=si), intent(in)         :: data_size
  real(kind=dr), dimension(:), pointer :: ptr ! local variable

  allocate(ptr(data_size)) ! the compiler does not deallocate the pointer
                           ! it is too complicated for the compiler to decide
                           ! if the allocated RAM is useful or not

  end subroutine memory_leak_with_pointer

  !=======================================!
  ! 2. subroutine: memory_leak_with_array !
  !=======================================!
 
  subroutine memory_leak_with_array_not_possible(data_size)

  use m_1_type_definitions

  implicit none

  integer(kind=si), intent(in)                   :: data_size
  real(kind=dr), dimension(:), allocatable       :: array_no_save ! local variable
  real(kind=dr), dimension(:), allocatable, save :: array_save    ! local variable

  if (allocated(array_no_save) .eqv. .false.) then

    allocate(array_no_save(1:data_size)) ! the compiler does deallocate the array
                                         ! not the Open64 compiler, so I put the if
  end if

  ! 2.

  if (allocated(array_save) .eqv. .false.) then

    allocate(array_save(1:data_size))    ! the compiler does not deallocate the array
                                         ! with save attribute

  end if

  end subroutine memory_leak_with_array_not_possible

  end module m_2_memory_leak

!======!
! FINI !
!======!
