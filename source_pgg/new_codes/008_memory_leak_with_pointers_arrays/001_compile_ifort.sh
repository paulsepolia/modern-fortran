#!/bin/bash

# 1. compile

  ifort -O3                      \
        -w                       \
        -e08                     \
        -static                  \
        -static-intel            \
        -parallel                \
        -par-threshold0          \
        m_1_type_definitions.f90 \
        m_2_memory_leak.f90      \
        driver_program.f90       \
        -o x_intel

# 2. clean

  rm *.mod
