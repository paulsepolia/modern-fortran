!===============================!
! Author: Pavlos G. Galiatsatos !
! Date: 2013/08/07              !
!===============================!

  program driver_program

  use m_1_type_definitions
  use m_2_memory_leak
 
  implicit none

  ! 1. local variables
  
  integer(kind=si), parameter :: MAX_DO_POINTER    = 1000000_si
  integer(kind=si), parameter :: DATA_SIZE_POINTER = 400_si
  integer(kind=si), parameter :: MAX_DO_ARRAY      = 100000000_si
  integer(kind=si), parameter :: DATA_SIZE_ARRAY   = 400_si
  integer(kind=si) :: i
  integer(kind=si) :: sentinel 
 
  ! 2. test the memory leak with pointer
 
  do i = 1, MAX_DO_POINTER

    call memory_leak_with_pointer(DATA_SIZE_POINTER)

  end do

  ! 3. prompt for input to notice the RAM leak

  write(*,*) " Notice the RAM wasted!"
  write(*,*)
  write(*,*) " Enter an integer to continue: "
  read(*,*) sentinel

  ! 4. test the memory leak with array - does not exist
  
  do i = 1, MAX_DO_ARRAY

   call memory_leak_with_array_not_possible(DATA_SIZE_ARRAY)

  end do
  
  ! 5.

  write(*,*) " Notice the total RAM wasted!"
  write(*,*)
  write(*,*) " Enter an integer to exit: "
  read(*,*) sentinel

  end program driver_program

!======!
! FINI !
!======!
