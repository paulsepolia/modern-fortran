!===============================!
! Author: Pavlos G. Galiatsatos !
! Date: 2013/08/05              !
!===============================!

  module m_2_integration_library

  use m_1_type_definitions

  implicit none

  type function_parameters
    ! No data - merely a placeholder
  end type function_parameters

  contains

  subroutine integrate_trapezoid(f, params, xmin, xmax, steps, the_result)

  use m_1_type_definitions

  implicit none

  interface
    real(kind=dr) function f(x, params)

      use m_1_type_definitions

      import function_parameters
 
      implicit none     

      real(kind=dr), intent(in)  :: x
      class(function_parameters) :: params

    end function f
  end interface

  class(function_parameters)   :: params
  real(kind=dr), intent(in)    :: xmin
  real(kind=dr), intent(in)    :: xmax
  integer(kind=si), intent(in) :: steps
  real(kind=dr), intent(out)   :: the_result
  integer(kind=si)             :: i
  real(kind=dr)                :: x
  real(kind=dr)                :: deltx

  if (steps <= 0_si) then
    the_result = 0.0_dr
    return
  endif

  deltx = (xmax - xmin)/steps

  the_result = (f(xmin, params) + f(xmax, params)) / 2.0_dr

  do i = 2, steps
    x = xmin + (i - 1) * deltx
    the_result = the_result + f(x,params)
  enddo

  the_result = the_result * deltx

  end subroutine integrate_trapezoid

  end module m_2_integration_library

!======!
! FINI !
!======!
