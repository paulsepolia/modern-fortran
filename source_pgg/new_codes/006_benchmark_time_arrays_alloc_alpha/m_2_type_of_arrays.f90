!===============================!
! Author: Pavlos G. Galiatsatos !
! Date: 2013/08/05              !
!===============================!

  module m_2_type_of_arrays

  use m_1_type_definitions

  implicit none

!==========!
! contains !
!==========!

  contains

  !  1.

  subroutine plain_arrays(data_array, sz, mean)

  use m_1_type_definitions

  implicit none
  
  real(kind=dr), dimension(:) :: data_array
  integer(kind=si)            :: sz
  real(kind=dr)               :: mean
  integer(kind=si)            :: i

  do i = 1, sz
    data_array(i) = real(i, kind=dr)
  end do

  mean = sum(data_array(1:sz)) / sz

  end subroutine plain_arrays

  !  2.

  subroutine auto_arrays(sz, mean)

  use m_1_type_definitions

  implicit none

  integer(kind=si)               :: sz
  real(kind=dr), dimension(1:sz) :: data_array  ! Local array
  real(kind=dr)                  :: mean
  integer(kind=si)               :: i

  do i = 1,sz
    data_array(i) = real(i, kind=dr)
  end do

  mean = sum(data_array(1:sz)) / sz

  end subroutine auto_arrays

  !  3.

  subroutine alloc_arrays(sz, mean)
  
  use m_1_type_definitions

  implicit none

  real(kind=dr), dimension(:), allocatable :: data_array  ! Local array
  integer(kind=si)                         :: sz
  real(kind=dr)                            :: mean
  integer(kind=si)                         :: i

  allocate(data_array(1:sz))

  do i = 1, sz
    data_array(i) = real(i, kind=dr)
  end do

  mean = sum(data_array(1:sz)) / sz

  if (allocated(data_array)) then
    deallocate(data_array)
  end if

  end subroutine alloc_arrays

  !  4.

  subroutine pointer_arrays(sz, mean)
   
  use m_1_type_definitions  

  implicit none

  real(kind=dr), dimension(:), pointer :: data_array ! Local array
  integer(kind=si)                     :: sz
  real(kind=dr)                        :: mean
  integer(kind=si)                     :: i

  allocate(data_array(1:sz))
 
  do i = 1,sz
    data_array(i) = real(i, kind=dr)
  end do
    
  mean = sum(data_array(1:sz)) / sz

  deallocate(data_array)

  end subroutine pointer_arrays

  end module m_2_type_of_arrays

!======!
! FINI !
!======!
