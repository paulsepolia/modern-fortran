!===============================!
! Author: Pavlos G. Galiatsatos !
! Date: 2013/08/05              !
!===============================!

  program driver_program
  
  use m_1_type_definitions
  use m_2_integration_library
  use m_3_functions_module

  implicit none

  type(my_function_parameters) :: params
  real(kind=dr)                :: xmin
  real(kind=dr)                :: xmax
  real(kind=dr)                :: the_result
  integer(kind=si)             :: steps

  params%a = 1.0_dr
  xmin     = 1.0_dr
  xmax     = 10.0_dr
  steps    = 10_si

  call integrate_trapezoid(f, params, xmin, xmax, steps, the_result)

  write(*,*) "Computed: ", the_result
  write(*,*) "Expected: ", 0.5*(xmin+xmax)*(xmax-xmin)

  end program driver_program

!======!
! FINI !
!======!
