!===============================!
! Author: Pavlos G. Galiatsatos !
! Date: 2013/08/05              !
!===============================!

  module m_3_functions_module
  
  use m_1_type_definitions  
  use m_2_integration_library, only: function_parameters

  implicit none

  type, extends(function_parameters) :: my_function_parameters
    real(kind=dr) :: a
  end type my_function_parameters

  contains

  real(kind=dr) function f(x, params)

  real(kind=dr), intent(in)  :: x
  class(function_parameters) :: params

  f = params%a * x

  end function f

  end module m_3_functions_module

!======!
! FINI !
!======!
