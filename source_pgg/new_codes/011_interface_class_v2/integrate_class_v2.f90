
module functions
    use integration_library, only: function_parameters

    implicit none

    type, extends(function_parameters) :: my_function_parameters
        real :: a
    end type my_function_parameters

contains
real function f( x, params )

    real, intent(in)              :: x
    class(my_function_parameters) :: params

    f = params%a * x

end function f

end module functions

