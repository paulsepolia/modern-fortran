!===============================!
! Author: Pavlos G. Galiatsatos !
! Date: 2013/08/06              !
!===============================!

  program driver_program

  use m_1_type_definitions
  use m_2_allocation

  implicit none

  !  1. local parameters

  integer(kind=si), parameter :: NUM_ALLOC  = 1000000_si
  integer(kind=si), parameter :: COEF_ALLOC = 100000_si
  integer(kind=si), parameter :: NUM_DO     = 10_si

  !  2. local variables

  integer(kind=si) :: sz
  integer(kind=si) :: i
  integer(kind=si) :: j
  integer(kind=si) :: time1
  integer(kind=si) :: time2
  integer(kind=si) :: total_noalloc
  integer(kind=si) :: total_alloc
  real(kind=dr)    :: r

  !  3. initializations

  total_noalloc = 0_si
  total_alloc   = 0_si

  !  4. main benchmark do-loop

  do j = 1, NUM_DO

    ! 4.a. No allocation
   
    call system_clock(time1)
        
    do i = 1, NUM_ALLOC
      call random_number(r)
      sz = int(COEF_ALLOC * r, kind=si)
      call alloc(-1)
    end do
        
    call system_clock(time2)
    
    total_noalloc = total_noalloc + (time2-time1)

    ! 4.b. allocations

    call system_clock(time1)
        
    do i = 1, NUM_ALLOC
      call random_number(r)
      sz = int(COEF_ALLOC * r, kind=si)
      call alloc(sz)
    end do
        
    call system_clock(time2)

    total_alloc = total_alloc + (time2-time1)
    
  end do

  !  5. some outputs

  write(*,*) ' 1 --> No allocation   --> ', total_noalloc
  write(*,*) ' 2 --> With allocation --> ', total_alloc

  end program driver_program

!======!
! FINI !
!======!
