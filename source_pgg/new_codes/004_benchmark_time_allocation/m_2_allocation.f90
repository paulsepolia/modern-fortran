!===============================!
! Author: Pavlos G. Galiatsatos !
! Date: 2013/08/06              !
!===============================!

  module m_2_allocation

  use m_1_type_definitions

  implicit none

  contains

  subroutine alloc(sz)

  integer(kind=si) :: sz
  real(kind=dr), dimension(:), allocatable :: array

  if (sz > 0) then
    allocate(array(1:sz))
  endif

  if (allocated(array)) then
    deallocate(array)
  end if 

  end subroutine alloc

  end module m_2_allocation

!======!
! FINI !    
!======!
