!===============================!
! Author: Pavlos G. Galiatsatos !
! Date: 2013/08/07              !
!===============================!

  program driver_program

  use m_1_type_definitions
  use m_2_extending_an_array

  implicit none

  ! 1. local variables and parameters

  integer(kind=si), parameter :: MAX_DO = 1000_si
  integer(kind=si), parameter :: OLD_SIZE = 1000_si
  integer(kind=si), parameter :: NEW_SIZE = 2000000_si
  real(kind=dr)    :: t1
  real(kind=dr)    :: t2
  integer(kind=si) :: i

  ! 2. extending the array size using the "move_alloc" function

  write(*,*) " 1 --> output from the move_alloc method"

  call cpu_time(t1)

  do i = 1, MAX_DO

    call ext_using_move_alloc(OLD_SIZE, NEW_SIZE)

  end do

  call cpu_time(t2)

  write(*,*) " 2 --> cpu time used using the move_alloc method --> ", (t2-t1)

  ! 3. extending the array size using the "naive" method

  write(*,*) " 3 --> output from the ext_naive_method"

  call cpu_time(t1)

  do i = 1, MAX_DO

    call ext_naive_method(OLD_SIZE, NEW_SIZE)
  
  end do

  call cpu_time(t2)

  write(*,*) " 4 --> cpu time used using the naive method --> ", (t2-t1)

  ! 4. extending the array size using the "pointer" method

  write(*,*) " 5 --> output from the pointer method"

  call cpu_time(t1)

  do i = 1, MAX_DO

    call ext_pointer_method(OLD_SIZE, NEW_SIZE)

  end do

  call cpu_time(t2)

  write(*,*) " 6 --> cpu time used using the pointer method --> ", (t2-t1)

  end program driver_program

!======!
! FINI !
!======!
