!===============================!
! Author: Pavlos G. Galiatsatos !
! Date: 2013/08/07              !
!===============================!

  module m_2_extending_an_array

  use m_1_type_definitions

  implicit none

!==========!
! contains !
!==========!

  contains

!=========================================================!
! A. subroutine: ext_using_move_alloc(old_size, new_size) !
!=========================================================!

  subroutine ext_using_move_alloc(old_size, new_size)
  
  use m_1_type_definitions

  implicit none

  !  1. interface variables
 
  integer(kind=si), intent(in) :: old_size
  integer(kind=si), intent(in) :: new_size

  !  2. local variables

  integer(kind=si)                         :: i
  real(kind=dr), dimension(:), allocatable :: array
  real(kind=dr), dimension(:), allocatable :: tmp_array
  integer(kind=si), save                   :: counter = 0_si
  
  !  3. allocate the array "array" with old size 

  allocate(array(1:old_size))

  !  4. initialize the array 

  do i = 1, old_size
    array(i) = real(i, kind=dr)
  end do

  !  5. allocate the tmp array with new size

  allocate(tmp_array(1:new_size))

  !  6. copy the data from the "array" to the "tmp_array"

  tmp_array(1:old_size) = array(1:old_size)

  !  7. deallocate the array: "array"

  deallocate(array)

  !  8. moving the "tmp_array" to the "array" 
  !     and deallocate the "tmp_array" in one step

  call move_alloc(tmp_array, array)

  !  9. test that "tmp_array" is deallocated now:
  
  if (allocated(tmp_array)) then
    write(*,*) "tmp_array is still allocated" 
  end if

  ! 10. initialize the rest elements in "array"
  
  do i = old_size+1, new_size
    array(i) = real(i, kind=dr)
  end do

  ! 11. some outputs

  if (counter == 0_si) then

    write(*,*) array(1)
    write(*,*) array(old_size)
    write(*,*) array(old_size+1)
    write(*,*) array(new_size)

  end if

  ! 12. clean RAM

  if (allocated(array)) then
    deallocate(array)
  end if

  if (allocated(tmp_array)) then
    deallocate(tmp_array)
  end if

  ! 13. increase the counter

  counter = counter + 1

  end subroutine ext_using_move_alloc

!=====================================================!
! 2. subroutine: ext_naive_method(old_size, new_size) !
!=====================================================!

  subroutine ext_naive_method(old_size, new_size)
  
  use m_1_type_definitions

  implicit none

  !  1. interface variables

  integer(kind=si), intent(in) :: old_size
  integer(kind=si), intent(in) :: new_size

  !  2. local variables

  integer(kind=si)                         :: i
  real(kind=dr), dimension(:), allocatable :: array
  real(kind=dr), dimension(:), allocatable :: tmp_array
  integer(kind=si), save                   :: counter = 0_si
  
  !  3. allocate the array "array" with old size 

  allocate(array(1:old_size))

  !  4. initialize the array "array" with old size 

  do i = 1, old_size
    array(i) = real(i, kind=dr)
  end do

  !  5. allocate the array "tmp_array" with new size

  allocate(tmp_array(1:new_size))

  !  6. copy the data from the "array" to the "tmp_array"

  tmp_array(1:old_size) = array(1:old_size)

  !  7. deallocate the array "array"

  deallocate(array)

  !  8. allocate the array "array" with new size

  allocate(array(1:new_size))

  !  9. copy back the "tmp_array" to "array"

  array = tmp_array

  ! 10. deallocate the "tmp_array"

  if (allocated(tmp_array)) then
    deallocate(tmp_array)
  end if

  ! 11. initialize the rest elements in "array"
  
  do i = old_size+1, new_size
    array(i) = real(i, kind=dr)
  end do

  ! 12. some outputs

  if (counter == 0) then

    write(*,*) array(1)
    write(*,*) array(old_size)
    write(*,*) array(old_size+1)
    write(*,*) array(new_size)

  end if

  ! 13. clean RAM

  if (allocated(array)) then
    deallocate(array)
  end if

  ! 14. increase the counter

  counter = counter + 1_si

  end subroutine ext_naive_method

!=======================================================!
! 3. subroutine: ext_pointer_method(old_size, new_size) !
!=======================================================!  

  subroutine ext_pointer_method(old_size, new_size)

  use m_1_type_definitions
  
  implicit none

  ! 1. interface variables
  
  integer(kind=si), intent(in) :: old_size
  integer(kind=si), intent(in) :: new_size

  ! 2. local variables

  real(kind=dr), dimension(:), pointer :: array
  real(kind=dr), dimension(:), pointer :: tmp_array
  integer(kind=si), save               :: counter = 0_si
  integer(kind=si)                     :: i
  
  !  3. allocate the array "array" with old size

  allocate(array(1:old_size))

  !  4. initialize the array "array" with old size

  do i = 1, old_size
    array(i) = real(i, kind=dr)
  end do

  !  5. allocate the array "tmp_array" with new size

  allocate(tmp_array(1:new_size))

  !  6. copy the data from the "array" to the "tmp_array"

  tmp_array(1:old_size) = array(1:old_size)

  !  7. deallocate the array "array"

  deallocate(array)

  !  8. "array" to point to "tmp_array"

  array => tmp_array

  !  9. initialize the rest elements in "array"
  
  do i = old_size+1, new_size
    array(i) = real(i, kind=dr)
  end do

  ! 10. some outputs

  if (counter == 0) then

    write(*,*) array(1)
    write(*,*) array(old_size)
    write(*,*) array(old_size+1)
    write(*,*) array(new_size)

  end if

  ! 11. clean the RAM: only one pointer I will delete: "array"

  deallocate(array)

  ! 12. increase the counter

  counter = counter + 1_si

  end subroutine ext_pointer_method

  end module m_2_extending_an_array

!======!
! FINI !
!======!
