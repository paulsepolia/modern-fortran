!===============================!
! Author: Pavlos G. Galiatsatos !
! Date: 2013/08/05              !
!===============================!

  program  driver_program
  
  use m_1_type_definitions
  use m_2_type_of_arrays

  implicit none

  !  1. local parameters

  integer(kind=si), parameter :: MAX_DIM  = 1000000_si
  integer(kind=si), parameter :: MAX_DO_A = 4_si
  integer(kind=si), parameter :: MAX_DO_B = 1000000_si

  !  2. local variables

  real(kind=dr), dimension(1:MAX_DIM), save :: rdata
  real(kind=dr)                             :: rmean
  integer(kind=si)                          :: sz
  integer(kind=si)                          :: total_plain
  integer(kind=si)                          :: total_auto
  integer(kind=si)                          :: total_alloc
  integer(kind=si)                          :: total_pointer
  integer(kind=si)                          :: time1
  integer(kind=si)                          :: time2
  integer(kind=si)                          :: i
  integer(kind=si)                          :: j

  !  3. main benchmark do-loop

  sz = 1_si
 
  do j = 1, MAX_DO_A

    total_plain   = 0_si
    total_auto    = 0_si
    total_alloc   = 0_si
    total_pointer = 0_si

    sz = sz * 10_si

    !  4.

    call system_clock(time1)

    do i = 1, MAX_DO_B
      call plain_arrays( rdata, sz, rmean )
    end do

    call system_clock(time2)
    total_plain = total_plain + (time2-time1)

    !  5.

    call system_clock(time1)

    do i = 1, MAX_DO_B
      call auto_arrays(sz, rmean)
    end do

    call system_clock(time2)
    total_auto = total_auto + (time2-time1)

    !  6.

    call system_clock(time1)

    do i = 1, MAX_DO_B
      call alloc_arrays(sz, rmean)
    end do

    call system_clock(time2)
    total_alloc = total_alloc + (time2-time1)

    !  7.

    call system_clock(time1)

    do i = 1, MAX_DO_B
      call pointer_arrays(sz, rmean)
    end do

    call system_clock(time2)
    total_pointer = total_pointer + (time2-time1)

    !  8. some outputs

    write(*,*) "Size: ", sz
    write(*,*) "Plain:           ", total_plain
    write(*,*) "Automatic:       ", total_auto
    write(*,*) "Allocate:        ", total_alloc
    write(*,*) "Pointer:         ", total_pointer

  end do

  end program driver_program

!======!
! FINI !
!======!
