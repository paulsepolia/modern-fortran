!===============================================!
! robust_stat.f90                               !
! Basic statistical parameters - robust version !
!===============================================!

  program driver_program_robust_stat

  use m_1_type_definitions

  implicit none

  !  1. local variables and parameters

  integer(kind=si)  :: MAX_DAT_ELEM = 1000000_si
  character(len=80) :: line
  real(kind=dr)     :: value_var
  real(kind=dr)     :: sumsq
  real(kind=dr)     :: sum_var
  integer(kind=si)  :: i
  integer(kind=si)  :: nodata
  integer(kind=si)  :: nomissing
  integer(kind=si)  :: noerrors
  integer(kind=si)  :: noempty
  integer(kind=si)  :: nolines
  integer(kind=si)  :: ierr
  logical(kind=si)  :: first_value = .true.
  real(kind=dr)     :: offset
  real(kind=dr)     :: vmean
  real(kind=dr)     :: stdev

  !  2. open file to write some test data

  open(unit   = 10,                 &
       file   = "robust_stat.data", &
       status = "replace",          &
       iostat = ierr)

  !  3. test if the file to write the data is okay

  if (ierr /= 0) then
  
    write(*,*) "Error opening file to write the input data - basic_stat.data"
    write(*,*) "Check what is happening!"
    stop

  end if

  !  4. write the test data

  do i = 1, MAX_DAT_ELEM

    write(unit=10, fmt=*) i

    if (mod(i,3) == 0) then
      write(unit=10, fmt=*) '?'
    end if
  
    if (mod(i,4) == 0) then
      write(unit=10, fmt=*) ' '
    end if

  end do

  !  5. close the unit

  close(unit=10, status="keep")

  !  6.

  open(unit = 10,                 &
       file = "robust_stat.data", &
       status = "old",            &
       iostat = ierr)

  if (ierr /= 0) then
  
    write(*,*) "Error opening file with input data - robust_stat.data"
    write(*,*) "Check that it exists"

    stop

  end if

  !  7.

  ! One value per line, ? means a missing value ...
  ! (Any other value that can not be read is regarded to be an error,
  ! empty lines are reported but not counted)

  sum_var   = 0.0_dr
  sumsq     = 0.0_dr
  nodata    = 0_si
  nomissing = 0_si
  nolines   = 0_si
  noempty   = 0_si
  noerrors  = 0_si
  offset    = 0_dr

  !  8. main read do-loop

  do

    read(unit=10, fmt='(a)', iostat = ierr) line

    if (ierr < 0) then
      ! End of file
      exit
    else if (ierr > 0) then
      ! Some reading error occurred - report it
      write(*,*) "Error reading line no.", nolines + 1_si
      write(*,*) "Skipping the rest of the file"
      exit
    else
      ! Get rid of tabs and carriage returns
      call cleanup_line(line)
      !
      ! Analyse the contents:
      ! - Empty line?
      ! - Missing value?
      ! - Not a valid number?
      ! - Valid number
      !
      nolines = nolines + 1_si

      if (line == ' ') then
        noempty = noempty + 1_si
        cycle
      end if

      if (adjustl(line) == '?') then
        nomissing = nomissing + 1_si
        cycle
      end if

      read(line, fmt=*, iostat = ierr) value_var

      if (ierr /= 0) then
        noerrors = noerrors + 1_si
        cycle
      end if

      !
      ! If the value is out of range, report it and
      ! skip it
      !
      if ((abs(value_var) > sqrt(huge(value_var))) .or. &
          ((abs(value_var) < sqrt(tiny(value_var))) .and. (abs(value_var) /= 0.0))) then
          
        write(*,*) "Value out of range: ", value_var, " - ignoring it!"
        nomissing = nomissing + 1_si
        cycle

      end if

      !
      ! We do have a valid value
      !
      if (first_value) then
        first_value = .false.
        offset = value_var
      end if

    sum_var    = sum_var    + (value_var - offset)
    sumsq  = sumsq  + (value_var - offset) ** 2_si
    nodata = nodata + 1_si
  end if
  end do

  close(unit=10, status="delete")

  !
  ! Report our findings
  !
 
  write(*,*) "Outcome:"
  write(*,*) "    Number of lines read:  ", nolines
  write(*,*) "    Number of empty lines: ", noempty
  write(*,*) "    Number of valid data:  ", nodata
  write(*,*) "    Number of missing data:", nomissing
  write(*,*) "    Number of invalid data:", noerrors
  write(*,*) " "

  if ( nodata > 0 ) then
    vmean = offset + sum_var / nodata
    write(*,*) "    Mean value:            ", vmean

    if ( nodata > 1 ) then
      stdev  = sqrt( (sumsq - sum_var ** 2_dr / nodata) / (nodata-1) )
      write(*,*) "    Standard deviation:    ", stdev
    else
      write(*,*) "    Standard deviation:    too few data"
    end if

  else

    write(*,*) "    Mean value:            too few data"

  end if

  contains

  subroutine cleanup_line(line)

  character(len=*), intent(inout) :: line
  logical                         :: found
  integer(kind=si)                :: k
  integer(kind=si)                :: i
  integer(kind=si), dimension(3)  :: chars = (/ 9, 10, 13 /)

  found = .true.

  do while (found)

    found = .false.
    !
    ! Remove any tabs, carriage returns and newlines
    !
    do i = 1, size(chars)
      k = index( line, achar(chars(i)) )
      if (k > 0) then
        found = .true.
        line(k:k) = ' '
      end if
    end do
  end do

  end subroutine cleanup_line

  end program driver_program_robust_stat

!======!
! FINI !
!======!
