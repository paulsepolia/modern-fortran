!==============================!
! basic_stat.f90               !
! Basic statistical parameters ! 
!==============================!

  program driver_program_basic_stat

  use m_1_type_definitions
  
  implicit none

  !  1. local variables and parameters

  integer(kind=si), parameter :: MAX_DAT_ELEM = 100000000_si
  real(kind=dr)               :: value_var
  real(kind=dr)               :: sumsq
  real(kind=dr)               :: sum_var
  real(kind=dr)               :: stdev
  real(kind=dr)               :: stdev2
  real(kind=dr)               :: vmean
  integer(kind=si)            :: i
  integer(kind=si)            :: nodata
  integer(kind=si)            :: nomissing
  integer(kind=si)            :: ierr

  !  2. open file to write some test data

  open(unit   = 10,                &
       file   = "basic_stat.data", &
       status = "replace",         &
       iostat = ierr)

  !  3. test if the file to write the data is okay

  if (ierr /= 0) then
  
    write(*,*) "Error opening file to write the input data - basic_stat.data"
    write(*,*) "Check what is happening!"
    stop

  end if

  !  4. write the test data

  do i = 1, MAX_DAT_ELEM
    write(unit=10, fmt=*) i
  end do

  !  5. close the unit

  close(unit=10, status="keep")

  !  6. open the file to read

  open(unit   = 10,                &
       file   = "basic_stat.data", &
       status = "old",             &
       iostat = ierr)

  !  7. test for succesful opening

  if (ierr /= 0) then

    write(*,*) "Error opening file with input data - basic_stat.data"
    write(*,*) "Check that it exists"
    stop

  end if

  !  8.

  ! One value per line, ? means a missing value ...
  ! (As a ? can not be read into a number, we consider each line that causes
  ! a read error to be a missing value)
 
  sum_var   = 0.0_dr
  sumsq     = 0.0_dr
  nodata    = 0_si
  nomissing = 0_si

  !  9.

  do
  
    read(unit=10, fmt=*, iostat = ierr) value_var

    if (ierr < 0) then

      ! End of file
      exit

    else if (ierr > 0) then

      ! Missing value
      nomissing = nomissing + 1_si
      cycle

    end if

      sum_var    = sum_var   + value_var
      sumsq  = sumsq  + value_var ** 2
      nodata = nodata + 1

  end do

  ! 10. close the unit

  close(unit=10, status="delete")

  ! 11.

  ! Report our findings
   
  write(*,*) "Outcome:"
  write(*,*) "    Number of valid data:  ", nodata
  write(*,*) "    Number of missing data:", nomissing
  write(*,*) " "

  if (nodata > 0) then
 
    vmean = sum_var / nodata
    write(*,*) "    Mean value:            ", vmean

    if (nodata > 1) then
      stdev2 = (sumsq - sum_var ** 2 / nodata) / (nodata-1)
      write(*,*) stdev2
      stdev  = sqrt((sumsq - sum_var ** 2 / nodata) / (nodata-1))
      write(*,*) "    Standard deviation:    ", stdev
    else
      write(*,*) "    Standard deviation:    too few data"
    endif
  else
    write(*,*) "    Mean value:            too few data"
  endif

  end program driver_program_basic_stat

!======!
! FINI !
!======!
