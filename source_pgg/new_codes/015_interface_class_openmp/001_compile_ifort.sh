#!/bin/bash

# 1. compile

  ifort -O3                  \
        -w                   \
        -e08                 \
        -static              \
        -static-intel        \
        -openmp              \
        -openmp-report2      \
        integrate_openmp.f90 \
        -o x_intel

# 2. clean

  rm *.mod
