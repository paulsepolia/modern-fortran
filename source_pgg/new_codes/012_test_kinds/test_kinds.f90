!===============================!
! Author: Pavlos G. Galiatsatos !
! Date: 2013/08/05              !
!===============================!

  module m_1_type_definitions

  implicit none
 
  !==================================================!
  ! 1. Common type definitions                       !         
  ! a. Here i define all the common type definitions !
  ! b. These kind types are used in any module       !
  !==================================================!

  integer, parameter :: qr = selected_real_kind(p=20,r=1000)
  integer, parameter :: dr = selected_real_kind(p=15,r=300)
  integer, parameter :: sr = selected_real_kind(p=6,r=30)
  integer, parameter :: di = selected_int_kind(18)
  integer, parameter :: si = selected_int_kind(9)
 
  end module m_1_type_definitions

!==============================================================================!
!==============================================================================!

  !  1.

  module single_real
 
  use m_1_type_definitions
 
  implicit none

  integer, parameter :: wp = kind(1.0_si)

  interface print_kind
    module procedure print_kind_wp
  end interface

  !==========!
  ! contains !
  !==========!

  contains

  subroutine print_kind_wp(x)
  real(kind=wp), intent(in) :: x

  write(*,*) 'Kind:', kind(x)
  end subroutine print_kind_wp

  end module single_real

!==============================================================================!
!==============================================================================!

  !  2.

  module double_real

  use m_1_type_definitions

  implicit none

  integer, parameter :: wp = kind(1.0_dr)

  interface print_kind
    module procedure print_kind_wp
  end interface

  !==========!
  ! contains !
  !==========!

  contains
  subroutine print_kind_wp(x)

    real(kind=wp), intent(in) :: x
    write(*,*) 'Kind:', kind(x)
  end subroutine print_kind_wp


  end module double_real

!==============================================================================!
!==============================================================================!

  !  3.

  module quad_real

  use m_1_type_definitions

  implicit none

  integer, parameter :: wp = kind(1.0_qr)

  interface print_kind
    module procedure print_kind_wp
  end interface

  !==========!
  ! contains !
  !==========!

  contains
  subroutine print_kind_wp(x)

    real(kind=wp), intent(in) :: x
    write(*,*) 'Kind:', kind(x)
  end subroutine print_kind_wp


  end module quad_real

!==============================================================================!
!==============================================================================!
  !  4.

  module unite
    use single_real
    use double_real
    use quad_real
  end module unite

!==============================================================================!
!==============================================================================!

  !  5.

  program test_kinds

  use m_1_type_definitions
  use unite

  call print_kind(1.0_sr)
  call print_kind(1.0_dr)
  call print_kind(1.0_qr)

  end program test_kinds

!======!
! FINI !
!======!
