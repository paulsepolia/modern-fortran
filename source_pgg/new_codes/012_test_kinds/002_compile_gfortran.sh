#!/bin/bash

# 1. compile

  gfortran-4.8.0 -O3            \
                 -Wall          \
                 -std=f2008     \
                 -static        \
                 test_kinds.f90 \
                 -o x_gnu

# 2. clean

  rm *.mod
