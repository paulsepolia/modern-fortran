!===============================!
! Author: Pavlos G. Galiatsatos !
! Date: 2013/08/05              !
!===============================!

  program driver_program
 
  use m_1_type_definitions
  use m_2_read_line

  implicit none

  !  1. local variables

  integer(kind=si)              :: lun
  logical                       :: success
  character(len=:), allocatable :: line

  !  2. local settings

  lun = 10
  open(lun, file = 'test_readline.inp')

  !  3. do-loop to read the line

  do

    call readline(lun, line, success)

    if (.not.success) then
      exit
    end if

    write(*,*) len(line), '>', line, '<'

    deallocate(line)

  end do

  end program driver_program

!======!
! FINI !
!======!
