! histogram_f90.f90 --
!
! Example belonging to "Modern Fortran in Practice" by Arjen Markus
!
! This work is licensed under the Creative Commons Attribution 3.0 Unported License.
! To view a copy of this license, visit http://creativecommons.org/licenses/by/3.0/
! or send a letter to:
! Creative Commons, 444 Castro Street, Suite 900, Mountain View, California, 94041, USA.
!
! Produce a simple histogram - Fortran 90 style
!
program hist
    implicit none

    integer, parameter :: maxdata = 1000
    integer, parameter :: nobnd   = 9

    real, dimension(maxdata) :: data
    real, dimension(nobnd)   :: bound = (/0.1, 0.3, 1.0, 3.0, 10.0, 30.0, 100.0, 300.0, 1000.0/)

    integer                  :: i, nodata, ierr

    open( 10, file = 'histogram.data', status = 'old', iostat = ierr )

    if ( ierr /= 0 ) then
        write( *, * ) 'file histogram.data could not be opened'
        stop
    endif

    open( 20, file = 'histogram.out' )

    do i = 1,size(data)
        read( 10, *, iostat = ierr ) data(i)

        if ( ierr > 0 ) then
            write( *, * ) 'Error reading the data!'
            stop
        elseif ( ierr < 0 ) then
            exit ! Reached the end of the file
        endif
    enddo

    close( 10 )
    nodata = i - 1

    call print_history( data(1:nodata), bound )

contains

! subroutine to print the histogram
!
subroutine print_history( data, bound )
    real, dimension(:), intent(in) :: data, bound

    integer :: i

    do i = 1,size(bound)
        write( 20, '(f10.2,i10)' ) bound(i), count( data <= bound(i) )
    enddo
end subroutine print_history

end program hist
