* histogram_f77.f
*
* Example belonging to "Modern Fortran in Practice" by Arjen Markus
*
* This work is licensed under the Creative Commons Attribution 3.0 Unported License.
* To view a copy of this license, visit http://creativecommons.org/licenses/by/3.0/
* or send a letter to:
* Creative Commons, 444 Castro Street, Suite 900, Mountain View, California, 94041, USA.
*
* Produce a simple histogram - FORTRAN 77 style
*
      PROGRAM HIST

      INTEGER MAXDATA
      PARAMETER (MAXDATA = 1000)

      INTEGER NOBND
      PARAMETER (NOBND = 9)
      REAL BOUND(NOBND)

      REAL DATA(MAXDATA)
      INTEGER I, NODATA

      DATA BOUND /0.1, 0.3, 1.0, 3.0, 10.0, 30.0, 100.0, 300.0, 1000.0/

      OPEN( 10, FILE = 'histogram.data', STATUS = 'OLD', ERR = 900 )
      OPEN( 20, FILE = 'histogram.out' )

      DO 110 I = 1,MAXDATA
          READ( 10, *, END = 120, ERR = 900 ) DATA(I)
  110 CONTINUE
*
  120 CONTINUE
      CLOSE( 10 )
      NODATA = I - 1

      CALL PRHIST( DATA, NODATA, BOUND, NOBND )
      STOP
*
*     File not found, and other errors
*
  900 CONTINUE
      WRITE( *, * ) 'File histogram.data could not be opened or some rea
     &ding error'

      END

*
* Subroutine to print the histogram
*
      SUBROUTINE PRHIST( DATA, NODATA, BOUND, NOBND )
      REAL DATA(*), BOUND(*)
      INTEGER NODATA, NOBND

      INTEGER I, J, NOHIST

      DO 120 I = 1,NOBND
         NOHIST = 0
         DO 110 J = 1,NODATA
             IF ( DATA(J) .LE. BOUND(I) ) THEN
                 NOHIST = NOHIST + 1
             ENDIF
  110    CONTINUE

         WRITE( 20, '(F10.2,I10)' ) BOUND(I), NOHIST
  120 CONTINUE

      END
